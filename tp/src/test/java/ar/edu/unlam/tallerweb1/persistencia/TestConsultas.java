package ar.edu.unlam.tallerweb1.persistencia;

import static org.assertj.core.api.Assertions.assertThat;

import org.hibernate.criterion.Restrictions;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import ar.edu.unlam.tallerweb1.SpringTest;
import ar.edu.unlam.tallerweb1.modelo.Ciudad;
import ar.edu.unlam.tallerweb1.modelo.Continente;
import ar.edu.unlam.tallerweb1.modelo.Pais;
import ar.edu.unlam.tallerweb1.modelo.Ubicacion;

public class TestConsultas extends SpringTest{
	
	@Test
	@Transactional @Rollback(true)
    public void testPaisesIdiomaIngles(){
		
		Continente miContinente1 = new Continente();
		miContinente1.setNombre("Europa");
		getSession().save(miContinente1);
		
		Continente miContinente2 = new Continente();
		miContinente1.setNombre("America");
		getSession().save(miContinente2);
		
		Ciudad miCiudad1 = new Ciudad();
		miCiudad1.setNombre("Londres");
		getSession().save(miCiudad1);
		
		Ciudad miCiudad2 = new Ciudad();
		miCiudad2.setNombre("Paris");
		getSession().save(miCiudad2);
		
		Ciudad miCiudad3 = new Ciudad();
		miCiudad3.setNombre("Buenos Aires");
		getSession().save(miCiudad3);
		
		Pais miPais1 = new Pais();
		miPais1.setNombre("Inglaterra");
		miPais1.setIdioma("Ingles");
		miPais1.setHabitantes(10);
		miPais1.setCapital(miCiudad1);
		miPais1.setContinente(miContinente1);
		getSession().save(miPais1);
		
		Pais miPais2 = new Pais();
		miPais2.setNombre("Francia");
		miPais2.setIdioma("Ingles");
		miPais2.setHabitantes(10);
		miPais2.setCapital(miCiudad2);
		miPais1.setContinente(miContinente1);
		getSession().save(miPais2);
		
		Pais miPais3 = new Pais();
		miPais3.setNombre("Argentina");
		miPais3.setIdioma("Espa�ol");
		miPais3.setHabitantes(10);
		miPais3.setCapital(miCiudad3);
		miPais1.setContinente(miContinente2);
		getSession().save(miPais3);
		
		List <Pais> miListaDePaisesQueHablanEnIngles = (List<Pais>) getSession().createCriteria(Pais.class)
				.add(Restrictions.eq("idioma", "Ingles")).list();
		
		  assertThat(miListaDePaisesQueHablanEnIngles.size()).isEqualTo(2);	
	}
	
	@Test
	@Transactional @Rollback(true)
    public void testPaisesDelContinenteEuropeo(){
		
		Continente Europa = new Continente();
		Europa.setNombre("Europa");
		getSession().save(Europa);
		
		Continente America = new Continente();
		America.setNombre("America");
		getSession().save(America);
		
		Ciudad miCiudad1 = new Ciudad();
		miCiudad1.setNombre("Londres");
		getSession().save(miCiudad1);
		
		Ciudad miCiudad2 = new Ciudad();
		miCiudad2.setNombre("Gales");
		getSession().save(miCiudad2);
		
		Ciudad miCiudad3 = new Ciudad();
		miCiudad3.setNombre("Buenos Aires");
		getSession().save(miCiudad3);
		
		Pais Inglaterra = new Pais();
		Inglaterra.setNombre("Inglaterra");
		Inglaterra.setIdioma("Ingles");
		Inglaterra.setHabitantes(10);
		Inglaterra.setCapital(miCiudad1);
		Inglaterra.setContinente(Europa);
		getSession().save(Inglaterra);
		
		Pais gales = new Pais();
		gales.setNombre("Gales");
		gales.setIdioma("Ingles");
		gales.setHabitantes(10);
		gales.setCapital(miCiudad2);
		gales.setContinente(Europa);
		getSession().save(gales);
		
		Pais miPais3 = new Pais();
		miPais3.setNombre("Argentina");
		miPais3.setIdioma("Espa�ol");
		miPais3.setHabitantes(10);
		miPais3.setCapital(miCiudad3);
		miPais3.setContinente(America);
		getSession().save(miPais3);
		
		
		
		List <Pais> miListaDePaisesEuropeos = (List<Pais>) getSession().createCriteria(Pais.class)
			.createAlias("continente", "continenteBuscado")
			.add(Restrictions.like("continenteBuscado.nombre","Europa"))
//				.add(Restrictions.eq("continente", miContinente1))
				.list();

			assertThat(miListaDePaisesEuropeos.size()).isEqualTo(2);
	}
		
		@Test
		@Transactional @Rollback(true)
	    public void testCapitalQueEstanAlNorteDelTropicoDeCancer(){
			
			Continente miContinente1 = new Continente();
			miContinente1.setNombre("Europa");
			getSession().save(miContinente1);
			
			Continente miContinente2 = new Continente();
			miContinente1.setNombre("America");
			getSession().save(miContinente2);
			
			Ubicacion miUbicacion1 = new Ubicacion();
			miUbicacion1.setLatitud(20);
			miUbicacion1.setLongitud(60);
			getSession().save(miUbicacion1);
			
			Ubicacion miUbicacion2 = new Ubicacion();
			miUbicacion2.setLatitud(10);
			miUbicacion2.setLongitud(30);
			getSession().save(miUbicacion2);
			
			Ubicacion miUbicacion3 = new Ubicacion();
			miUbicacion3.setLatitud(50);
			miUbicacion3.setLongitud(90);
			getSession().save(miUbicacion3);
			
			Ciudad miCiudad1 = new Ciudad();
			miCiudad1.setNombre("Londres");
			miCiudad1.setUbicacion(miUbicacion1);
			getSession().save(miCiudad1);
			
			Ciudad miCiudad2 = new Ciudad();
			miCiudad2.setNombre("Washington");
			miCiudad2.setUbicacion(miUbicacion2);
			getSession().save(miCiudad2);
			
			Ciudad miCiudad3 = new Ciudad();
			miCiudad3.setNombre("Buenos Aires");
			miCiudad3.setUbicacion(miUbicacion3);
			getSession().save(miCiudad3);
			
			Pais miPais1 = new Pais();
			miPais1.setNombre("Inglaterra");
			miPais1.setIdioma("Ingles");
			miPais1.setHabitantes(10);
			miPais1.setCapital(miCiudad1);
			miPais1.setContinente(miContinente1);
			getSession().save(miPais1);
			
			Pais miPais2 = new Pais();
			miPais2.setNombre("Estados Unidos");
			miPais2.setIdioma("Ingles");
			miPais2.setHabitantes(10);
			miPais2.setCapital(miCiudad2);
			miPais1.setContinente(miContinente1);
			getSession().save(miPais2);
			
			Pais miPais3 = new Pais();
			miPais3.setNombre("Argentina");
			miPais3.setIdioma("Espa�ol");
			miPais3.setHabitantes(10);
			miPais3.setCapital(miCiudad3);
			miPais1.setContinente(miContinente2);
			getSession().save(miPais3);
			
			List <Pais> miListaDeCapitalesAlNorteDelTropicoDeCancer = (List<Pais>) getSession().createCriteria(Pais.class)
					.createAlias("capital", "capitalBuscada")
					.createAlias("capitalBuscada.ubicacion", "ubicacionBuscada")
					.add(Restrictions.gt("ubicacionBuscada.latitud",20)).list();
			
				assertThat(miListaDeCapitalesAlNorteDelTropicoDeCancer.size()).isEqualTo(1);
		}
		
		@Test
		@Transactional @Rollback(true)
	    public void CiudadesQueEstanEnElHemisferioSur(){
			
			Continente miContinente1 = new Continente();
			miContinente1.setNombre("Europa");
			getSession().save(miContinente1);
			
			Continente miContinente2 = new Continente();
			miContinente1.setNombre("America");
			getSession().save(miContinente2);
			
			Ubicacion miUbicacion1 = new Ubicacion();
			miUbicacion1.setLatitud(20);
			miUbicacion1.setLongitud(60);
			getSession().save(miUbicacion1);
			
			Ubicacion miUbicacion2 = new Ubicacion();
			miUbicacion2.setLatitud(10);
			miUbicacion2.setLongitud(30);
			getSession().save(miUbicacion2);
			
			Ubicacion miUbicacion3 = new Ubicacion();
			miUbicacion3.setLatitud(50);
			miUbicacion3.setLongitud(90);
			getSession().save(miUbicacion3);
			
			Ciudad miCiudad1 = new Ciudad();
			miCiudad1.setNombre("Londres");
			miCiudad1.setUbicacion(miUbicacion1);
			getSession().save(miCiudad1);
			
			Ciudad miCiudad2 = new Ciudad();
			miCiudad2.setNombre("Washington");
			miCiudad2.setUbicacion(miUbicacion2);
			getSession().save(miCiudad2);
			
			Ciudad miCiudad3 = new Ciudad();
			miCiudad3.setNombre("Buenos Aires");
			miCiudad3.setUbicacion(miUbicacion3);
			getSession().save(miCiudad3);
			
			Pais miPais1 = new Pais();
			miPais1.setNombre("Inglaterra");
			miPais1.setIdioma("Ingles");
			miPais1.setHabitantes(10);
			miPais1.setCapital(miCiudad1);
			miPais1.setContinente(miContinente1);
			getSession().save(miPais1);
			
			Pais miPais2 = new Pais();
			miPais2.setNombre("Estados Unidos");
			miPais2.setIdioma("Ingles");
			miPais2.setHabitantes(10);
			miPais2.setCapital(miCiudad2);
			miPais1.setContinente(miContinente1);
			getSession().save(miPais2);
			
			Pais miPais3 = new Pais();
			miPais3.setNombre("Argentina");
			miPais3.setIdioma("Espa�ol");
			miPais3.setHabitantes(10);
			miPais3.setCapital(miCiudad3);
			miPais1.setContinente(miContinente2);
			getSession().save(miPais3);
			
			List <Ciudad> miListaDeCiudadesQueEstanEnElHemisferioSur = (List<Ciudad>) getSession().createCriteria(Ciudad.class)
					.createAlias("ubicacion", "ubicacionBuscada")
					.add(Restrictions.lt("ubicacionBuscada.latitud",40)).list();
			
				assertThat(miListaDeCiudadesQueEstanEnElHemisferioSur.size()).isEqualTo(2);
		}
		
		
}
