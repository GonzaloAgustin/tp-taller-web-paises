package ar.edu.unlam.tallerweb1.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ar.edu.unlam.tallerweb1.modelo.Usuario;

// Usando path variables, hacer un action que reciba una operacion y una cadena de caracteres y que 
// lleve a una vista que muestra las siguiente franse "El resultado de <nombre-operacion> sobre<cadena>
// <cadena-resultado>", donde la <cadena-resultado> es la cadena resultante de aplicar la 
//operacion a la cadena recibida por parametro. Las operaicon pueden ser:
//pasarAMayusucula, pasarAMinuscula,invertirOrden, cantidadDeCaracteres(En ese caso devuelve numero)
@Controller
public class ControladorCaracteres{

	@RequestMapping("/strings/{x}/{y}")
	public ModelAndView Cambios(@PathVariable String x,@PathVariable String y) {

		ModelMap miModelo = new ModelMap();
		
		miModelo.put("x", x);
		miModelo.put("y", y);
		
		if(y.equals("pasarAMayuscula")) {
		 String resultado = x.toUpperCase();
		 miModelo.put("resultado",resultado);}
		if(y.equals("pasarAMinuscula")) {
			 String resultado = x.toLowerCase();
			 miModelo.put("resultado",resultado);}
		if(y.equals("invertirOrden")) {
			String sCadenaInvertida = "";
			for (int xd=x.length()-1;xd>=0;xd--) {
				sCadenaInvertida = sCadenaInvertida + x.charAt(xd);
				}
			miModelo.put("resultado",sCadenaInvertida);}
		if(y.equals("contarCaracteres")) {
			 Integer resultado = x.length();
			 miModelo.put("resultado",resultado);}
			 
	
		
		return new ModelAndView("stringsTp",miModelo);
	}

}
